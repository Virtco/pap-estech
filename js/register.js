
$(document).ready(function () {
    $("#formularioCadastro").validate({
        rules: {
            primeiroNome: {
                required: true
            },
            ultimoNome: {
                required: true
            },
            cpf: {
                required: true
            },
            endereco: {
                required: true
            },
            bairro: {
                required: true
            },
            cidade: {
                required: true
            },
            estado: {
                required: true
            }
        }
    });

    $("#cpf").mask("000.000.000-00");
    $("#cep").mask("00000-000");
    $("#numero").mask("0000");
    $("#telefone").mask("(11) 0000-0000");

    $.fn.serializeObject = function () {
        var o = [];
        var a = $("form").serializeArray();
        $.each(a, function (i, field) {
            var j = new Object();
            j.name = field.name;
            j.value = field.value;
            o.push(j)
        });
        return o;
    };

    $("#formularioCadastro").on('submit', function () {
        if (!validar())
            return;
        $.ajax({
            url: "Services/FormCadastro.php",
            type: 'POST',
            data: $("#formularioCadastro").serializeObject()
        })
            .fail(function (error) {
                $("#resposta")[0].innerHTML = "Favor tente mais tarde!";
            })
            .done(function (msg) {
                if (msg == 0) {
                    $("#resposta")[0].innerHTML = "Problema no cadastro";
                    document.getElementById("mensagemContato").style.display = "inline";
                    return;
                } else
                    $("#resposta")[0].innerHTML = "Cadastrado com sucesso!";
                document.getElementById("divCadastro").style.display = "none";
                document.getElementById("mensagemCadastro").style.display = "inline";
            });
    });
});
function validar() {
    if (document.getElementById("primeiroNome").value === '') {
        return false;
    }
    if (document.getElementById("ultimoNome").value === '') {
        return false;
    }
    if (document.getElementById("cpf").value === '') {
        return false;
    }
    if (document.getElementById("cep").value === '') {
        return false;
    }
    if (document.getElementById("endereco").value === '') {
        return false;
    }
    if (document.getElementById("numero").value === '') {
        return false;
    }
    if (document.getElementById("bairro").value === '') {
        return false;
    }
    if (document.getElementById("cidade").value === '') {
        return false;
    }
    if (document.getElementById("estado").value === '') {
        return false;
    }
    if (document.getElementById("email").value === '') {
        return false;
    }
    if (document.getElementById("telefone").value === '') {
        return false;
    }
    if (document.getElementById("login").value === '') {
        return false;
    }
    if (document.getElementById("senha").value === '') {
        return false;
    }
    return true;
}
