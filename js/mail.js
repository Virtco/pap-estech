
$(document).ready(function () {

    $.fn.serializeObject = function () {
        var o = [];
        var a = $("form").serializeArray();
        $.each(a, function (i, field) {
            var j = new Object();
            j.name = field.name;
            j.value = field.value;
            o.push(j)
        });
        return o;
    };

    $("#formularioContato").validate({
        rules: {
            email: {
                required: true
            },
            subject: {
                required: true
            },
            message: {
                required: true
            }
        }
    });

    $("#formularioContato").on('submit', function () {
        if (!validar())
            return;
        $.ajax({
            url: "Services/FormContato.php",
            type: 'POST',
            data: $("#formularioContato").serializeObject()
        })
            .fail(function (error) {
                $("#resposta")[0].innerHTML = "Favor tente mais tarde!";
            })
            .done(function (msg) {
                if (msg == 0) {
                    $("#resposta")[0].innerHTML = "Falha no contato, favor tente mais tarde!";
                    document.getElementById("mensagemContato").style.display = "inline";
                    return;
                } else
                    $("#resposta")[0].innerHTML = "Retornaremos em breve! Obrigado!";
                document.getElementById("formularioContato").style.display = "none";
                document.getElementById("mensagemContato").style.display = "inline";
            });
    });

});
function validar() {
    if (document.getElementById("email").value === '') {
        return false;
    }
    if (document.getElementById("subject").value === '') {
        return false;
    }
    if (document.getElementById("text").value === '') {
        return false;
    }
    return true;
}