$(document).ready(
    function () {
    $.fn.serializeObject = function () {
        var o = [];
        var a = $("form").serializeArray();
        $.each(a, function (i, field) {
            var j = new Object();
            j.name = field.name;
            j.value = field.value;
            o.push(j)
        });
        return o;
    };
    
    $("#formLogin").validate({
        rules: {
            login: {
                required: true
            },
            senha: {
                required: true
            }
        }
    });

    $("#formLogin").on('submit', function () {
        if (!validar())
            return;
        $.ajax({
            url: "Services/login.php",
            type: 'POST',
            data: $("#formLogin").serializeObject()
        })
            .fail(function (error) {
                $("#resposta")[0].innerHTML = "Email ou senha errados!";
            })
            .done(function (msg) {
                if (msg == 0) {
                    $("#resposta")[0].innerHTML = "Login ou senha errados";
                    document.getElementById("mensagemContato").style.display = "inline";
                    return;
                } else
                    $("#resposta")[0].innerHTML = msg;
                document.getElementById("divLogin").style.display = "none";
                document.getElementById("novoCad").style.display = "none";
                document.getElementById("mensagemContato").style.display = "inline";
            });
    });
});

function validar() {
    if (document.getElementById("login").value === '') {
        return false;
    }
    if (document.getElementById("senha").value === '') {
        return false;
    }
    return true;
}

function hide() {
    document.getElementById("mensagemContato").style.display = "none";
}