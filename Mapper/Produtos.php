<?php

include __DIR__ . '/../Controller/Query.php';

class Produtos {

    protected $select;

    function __construct() {
        $this->select = new Query();
    }

    public function getProdutos($where) {
        return $this->select->executeQuery(
                        "SELECT * FROM produtos $where;"
        );
    }

    public function getProdutoPorId($id) {
        return $this->select->executeQuery(
                        "SELECT * FROM produtos where id IN ($id);"
        );
    }

}
