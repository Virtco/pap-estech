<?php
include __DIR__ . '/Mapper/Produtos.php';
$mapperProdutos = new Produtos();
$resultado = $mapperProdutos->getProdutoPorId($_GET['id']);
?>
<html>
    <?php include __DIR__ . '/View/head.php'; ?>
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <script defer src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/single.js"></script>
    <script src="js/imagezoom.js"></script>
    <body>
        <?php include __DIR__ . '/View/header.php'; ?>        
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Início</a></li>
                    <li class="active">Produto</li>
                </ol>
            </div>
        </div>
        <div class="single">
            <div class="container">
                <div class="col-md-8 single-right">
                    <div class="col-md-5 single-right-left animated wow slideInUp" data-wow-delay=".5s">
                        <div class="flexslider">
                            <ul class="slides">
                                <li data-thumb="https://tpws.com.br/imagens/admin/logo/produtos/<?= $resultado[0]['imagem']; ?>">
                                    <div class="thumb-image"> <img src="https://tpws.com.br/imagens/admin/logo/produtos/<?= $resultado[0]['imagem']; ?>" data-imagezoom="true" class="img-responsive"> </div>
                                </li>
                                <li data-thumb="https://tpws.com.br/imagens/admin/logo/produtos/<?= $resultado[0]['imagem']; ?>">
                                    <div class="thumb-image"> <img src="https://tpws.com.br/imagens/admin/logo/produtos/<?= $resultado[0]['imagem']; ?>" data-imagezoom="true" class="img-responsive"> </div>
                                </li>
                                <li data-thumb="https://tpws.com.br/imagens/admin/logo/produtos/<?= $resultado[0]['imagem']; ?>">
                                    <div class="thumb-image"> <img src="https://tpws.com.br/imagens/admin/logo/produtos/<?= $resultado[0]['imagem']; ?>" data-imagezoom="true" class="img-responsive"> </div>
                                </li> 
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7 single-right-left simpleCart_shelfItem animated wow slideInRight" data-wow-delay=".5s">
                        <div class="occasion-cart">
                            <h3><?= $resultado[0]['nome'] ?></h3>
                            <h4>
                                <span class="item_price">
                                    R$<?= number_format(floatval($resultado[0]['preco']), 2); ?>
                                </span>
                                <div class="description">
                                    <h5><i>Descrição</i></h5>
                                    <p><?= $resultado[0]['descricao'] ?></p>
                                </div>
                                <div class="color-quality">

                                    <div class="color-quality-left">
                                        <h5>Quantidade :</h5>
                                        <input class="item_quantity" type="number" id="country1" min="1" max="99"/>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                                <br>
                                <span class="item_image" style="display:none">https://tpws.com.br/imagens/admin/logo/produtos/<?= $resultado[0]['imagem']; ?></span>
                                <span class="item_name" style="display:none"><?= $resultado[0]['nome']; ?></span>
                                <a class="item_add" href="#">Adicionar ao carrinho </a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(__DIR__ . '/View/footer.php'); ?>
    </body>
</html>