<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/View/head.php'; ?>
    <script type="text/javascript" src="js/login.js"></script>
    <body>
        <?php include __DIR__ . '/View/header.php'; ?>
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Início</a></li>
                    <li class="active">Página de Login</li>
                </ol>
            </div>
        </div>
        <div class="login">
            <div class="container">
                <h3 class="animated wow zoomIn" data-wow-delay=".5s">Login</h3>
                <p class="est animated wow zoomIn" data-wow-delay=".5s">Faça seu login para acessar seu carrinho e fazer suas compras agora!</p>
                <div class="animated" id="mensagemContato" style="display:none" class="col-md-8" data-wow-delay=".5s">
                    <h1><p style="font-size:100%" id="resposta">Login efetuado com sucesso!</p></h1>
                </div>
                <div id="divLogin" class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
                    <form id="formLogin" action="javascript:;" >
                        <input type="text" onblur="hide()" id="login" name="login"  placeholder="Login" required=" " >
                        <input type="password" onblur="hide()" id="senha" name="senha" placeholder="Senha" required=" " >
                        <!--<div class="forgot">
                            <a href="#">Esqueceu a senha?</a>
                        </div>-->
                        <input type="submit" value="Entrar">
                    </form>
                </div>
                <div id="mensagemContato" class="register-home" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
                    <a id="botao" href="index.php">Início</a>
                </div>
                <div id="novoCad">
                <h4 class="animated wow slideInUp" data-wow-delay=".5s">Novo por aqui?</h4>
                <p class="animated wow slideInUp" data-wow-delay=".5s"><a href="register.php">Cadastre-se agora!</a> Ou clique para voltar ao <a href="index.php">Início<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
                </div>
            </div>
        </div>
        <?php include(__DIR__ . '/View/footer.php'); ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $.fn.serializeObject = function () {
                    var o = [];
                    var a = $("form").serializeArray();
                    $.each(a, function (i, field) {
                        var j = new Object();
                        j.name = field.name;
                        j.value = field.value;
                        o.push(j)
                    });
                    return o;
                };
                $("#formLogin").validate({
                    rules: {
                        login: {
                            required: true
                        },
                        senha: {
                            required: true
                        }
                    }
                });

                $("#formLogin").on('submit', function () {
                    if(!validar())
                        return;
                    $.ajax({
                        url: "Services/login.php",
                        type: 'POST',
                        data: $("#formLogin").serializeObject()
                    })
                    .fail(function (error) {
                        $("#resposta")[0].innerHTML = "Email ou senha errados!";
                    })
                    .done(function (msg) {
                        if(msg == 0){
                           $("#resposta")[0].innerHTML = "Login ou senha errados";
                            document.getElementById("mensagemContato").style.display = "inline";
                            return;
                        } else
                            $("#resposta")[0].innerHTML = msg;
                        document.getElementById("divLogin").style.display = "none";
                        document.getElementById("novoCad").style.display = "none";
                        document.getElementById("mensagemContato").style.display = "inline";
                    });
                });
            });
        </script>
        <script type="text/javascript">
            function validar() {
                if (document.getElementById("login").value === '') {
                    return false;
                }
                if (document.getElementById("senha").value === '') {
                    return false;
                }
                return true;
            }

            function hide() {
                document.getElementById("mensagemContato").style.display = "none";
            }

        </script>
    </body>
</html>