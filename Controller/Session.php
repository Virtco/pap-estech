<?php

class Session {

    public function get($chave) {
        if(isset($_SESSION[$chave])){
            return $_SESSION[$chave];
        } else {
            return [];
        }
    }

    public function set($chave, $valor) {
            $_SESSION[$chave] = $valor;
    }

}
?>

