<?php

class Query {

    private $lastId = 0;

    public function getConexao() {

        $servername = "mysql762.umbler.com:41890";
        $username = "quim_pulse_pap";
        $password = "pap123456";
        $dbname = "quim_pulse_pap";

// Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
        if ($conn->connect_error) {
            return "Connection failed: " . $conn->connect_error;
        }
//echo "Connected successfully";
        return $conn;
    }

    public function executeQuery($select = "SELECT * FROM produtos;", $objeto = "") {

        try {
            $retorno = [];
            $conexao = $this->getConexao();
            $result = mysqli_query($conexao, $select);

            if (mysqli_num_rows($result) > 0) {
                // output data of each row
                while ($row = mysqli_fetch_assoc($result)) {
                    $retorno[] = $row;
                    //$retorno[] = new {$objeto}()
                    //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
                }
            } else {
                echo "0 results";
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        } finally {
            mysqli_close($conexao);
        }
        return $retorno;
    }

    public function executeQueryInsert($query, $objeto = "") {

        try {
            $retorno = [];
            $conexao = $this->getConexao();
            $retorno = mysqli_query($conexao, $query);
            $this->lastId = mysqli_insert_id($conexao);
            return $retorno;

        } catch (Exception $exc) {
            echo $exc->getMessage();
        } finally {
            mysqli_close($conexao);
        }
        return $retorno;
    }
    
    public function login($select = "", $objeto = "") {

        try {
            $retorno = "";
            $conexao = $this->getConexao();
            $result = mysqli_query($conexao, $select);
            
            if (mysqli_num_rows($result) > 0) {
                // output data of each row
                while ($row = mysqli_fetch_assoc($result)) {
                    $result->data_seek(0);
                    $datarow = $result->fetch_array();
                    $retorno = $datarow['razao_social']; 
                    //$retorno[] = new {$objeto}()
                    //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
                }
                echo "Olá " . $retorno;
            } else {
                echo "Login ou senha invalidos";
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        } finally {
            mysqli_close($conexao);
        }
    }

    public function insert($insert) {
        try {
            $retorno = '';
            $conexao = $this->getConexao();

            if ($this->executeQueryInsert($insert) === TRUE) {
                $retorno = "New record created successfully";
            } else {
                $retorno = "Error: " . $insert . "<br>" . $conexao->error;
            }
        } catch (Exception $exc) {
            $retorno = $exc->getMessage();
        } finally {
            mysqli_close($conexao);
        }
        echo $retorno;
    }

    public function lastId(){
        return $this->lastId;
    }

}
?>

