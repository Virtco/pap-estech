<?php
include __DIR__ . '/Controller/Query.php';
$select = new Query();
$resultado = $select->executeQuery();
?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/View/head.php'; ?>
    <body>
        <?php include __DIR__ . '/View/header.php'; ?>
        <div class="banner">
            <div class="container">
                <div class="banner-info animated wow zoomIn"  data-wow-delay=".5s">
                    <div class="wmuSlider example1">
                    </div>
                    <script src="js/jquery.wmuSlider.js"></script> 
                    <script>
                        $('.example1').wmuSlider();
                    </script> 
                </div>
            </div>
        </div>
        <div class="banner-bottom">
            <div class="container"> 
                <div class="banner-bottom-grids">
                    <div class="banner-bottom-grid-left animated wow slideInLeft" data-wow-delay=".5s">
                        <div class="grid">
                            <figure class="effect-julia">
                                <img src="images/4.jpg" alt=" " class="img-responsive" />
                                <figcaption>
                                    <h3>Melhor <span> Loja</span><i> de suplementos!</i></h3>
                                    <div>
                                        <p>Suplementos de qualidade!</p>
                                        <p>Certificados e Comprovados!</p>
                                        <p>Adquira já o seu!</p>
                                    </div>
                                </figcaption>			
                            </figure>
                        </div>
                    </div>
                    <div class="banner-bottom-grid-left1 animated wow slideInUp" data-wow-delay=".5s">
                        <div class="banner-bottom-grid-left-grid left1-grid grid-left-grid1">
                            <div class="banner-bottom-grid-left-grid1">
                                <img src="images/1.jpg" alt=" " class="img-responsive" />
                            </div>
                            <div class="banner-bottom-grid-left1-pos">
                                <p>Conheça a produção!</p>
                            </div>
                        </div>
                        <div class="banner-bottom-grid-left-grid left1-grid grid-left-grid1">
                            <div class="banner-bottom-grid-left-grid1">
                                <img src="images/2.jpg" alt=" " class="img-responsive" />
                            </div>
                            <div class="banner-bottom-grid-left1-position">
                                <div class="banner-bottom-grid-left1-pos1">
                                    <p style="max-height: 45px">Desconto 50%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="banner-bottom-grid-right animated wow slideInRight" data-wow-delay=".5s">
                        <div class="banner-bottom-grid-left-grid grid-left-grid1">
                            <div class="banner-bottom-grid-left-grid1">
                                <img src="images/3.jpg" alt=" " class="img-responsive" />
                            </div>
                            <div class="banner-bottom-grid-left1-pos">
                                <p>Novas combinações</p>
                            </div>
                            <div class="grid-left-grid1-pos">

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <div class="new-collections">
            <div class="container">
                <h3 class="animated wow zoomIn" data-wow-delay=".5s">Lançamentos</h3>
                <p class="est animated wow zoomIn" data-wow-delay=".5s">Acompanhe os lançamentos da Pulse e esteja por dentro de todos os benefícios de nossos produtos.</p>
                <div class="new-collections-grids">

                    <?php foreach ($resultado as $key => $value): ?>
                        <?php if ($key <= 3): ?>
                            <div class="col-md-3 new-collections-grid" style="margin-bottom:15px;">

                                <div class="new-collections-grid1 animated wow slideInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
                                    <div class="new-collections-grid1-image">
                                        <a href="single.php" class="product-image">
                                            <img style="max-height:128px;min-height:128px;" 
                                                 src="https://tpws.com.br/imagens/admin/logo/produtos/<?= $value['imagem']; ?>" 
                                                 alt=" " class="img-responsive">
                                        </a>
                                        
                                        <div class="new-collections-grid1-image-pos">
                                            <a class="item_id" id="=<?= $value['id']; ?>" href="single.php?id=<?= $value['id']; ?>">Veja Mais</a>
                                        </div>
                                    </div>
                                    <h4><a class="item_name"><?= $value['nome']; ?></a></h4>
                                    <p><?= $value['descricao']; ?></p>
                                    <div class="new-collections-grid1-left simpleCart_shelfItem">
                                        <span class="item_image" style="display:none">https://tpws.com.br/imagens/admin/logo/produtos/<?= $value['imagem']; ?></span>
                                        <span class="item_name" style="display:none"><?= $value['nome']; ?></span>
                                        <p><i></i> 
                                            <span class="item_price">
                                                R$<?= number_format(floatval($value['preco']), 2); ?>
                                            </span>
                                            <a class="item_add" href=#>  Carrinho!</a>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    <div class="clearfix"> </div>
                </div>

                <div class="new-collections-grids">
                    <?php foreach ($resultado as $key => $value): ?>
                        <?php if ($key >= 4 && $key <= 7): ?>
                            <div class="col-md-3 new-collections-grid" style="margin-bottom:15px;">

                                <div class="new-collections-grid1 animated wow slideInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
                                    <div class="new-collections-grid1-image">
                                        <a href="single.php" class="product-image">
                                            <img style="max-height:128px;min-height:128px;" 
                                                 src="https://tpws.com.br/imagens/admin/logo/produtos/<?= $value['imagem']; ?>" 
                                                 alt=" " class="img-responsive">
                                        </a>
                                        <div class="new-collections-grid1-image-pos">
                                            <a href="single.php?id=<?= $value['id']; ?>">Veja Mais</a>
                                        </div>
                                    </div>
                                    <h4><a class="item_name"><?= $value['nome']; ?></a></h4>
                                    <p><?= $value['descricao']; ?></p>
                                    <div class="new-collections-grid1-left simpleCart_shelfItem">
                                        <span class="item_image" style="display:none">images/produtos/<?= $value['imagem']; ?></span>
                                        <span class="item_name" style="display:none"><?= $value['nome']; ?></span>
                                        <p><i></i> 
                                            <span class="item_price">
                                                R$<?= number_format(floatval($value['preco']), 2); ?>
                                            </span>
                                            <a class="item_add" href="#">Carrinho!</a>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div class="clearfix"> </div>
                </div>


            </div>
        </div>
        <div class="timer">
            <div class="container">
                <div class="timer-grids">
                    <div class="col-md-8 timer-grid-left animated wow slideInLeft" data-wow-delay=".5s">
                        <h3><a href="products.php">Caffeine Addiction - Seu furuto vício saudável da Pulse!</a></h3>
                        <div class="new-collections-grid1-left simpleCart_shelfItem timer-grid-left-price">
                            <p><span class="item_price">R$180,00</span></p>
                            <h4>Com a nova base cafeinada da pulse, seus dias ficaram mais longos e produtivos!
                                Nunca mais se preocupe com acordar tarde e ter seus dias arruinados por mau humor!
                                Prove este suplemento e você terá seu vigor como se todo dia fosse uma manhã de sol!

                            </h4>
                            <p><a class="item_add timer_add">Em Breve! </a></p>
                        </div>
                        <div id="counter"> </div>
                        <script src="js/jquery.countdown.js"></script>
                        <script src="js/script.js"></script>
                    </div>
                    <div class="col-md-4 timer-grid-right animated wow slideInRight" data-wow-delay=".5s">
                        <div class="timer-grid-right1">
                            <img src="images/17.jpg" alt=" " class="img-responsive" />
                            <div class="timer-grid-right-pos">
                                <h4>Lançamento em Breve!</h4>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <?php include(__DIR__ . '/View/footer.php'); ?>
    </body>
</html>
