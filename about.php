<html>
    <?php include __DIR__ . '/View/head.php'; ?>
    <body>
        <?php include __DIR__ . '/View/header.php'; ?>
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Início</a></li>
                    <li class="active">Sobre Nós</li>
                </ol>
            </div>
        </div>
        <div class="typo">
            <div class="container">
                <div class="typo-grids">
                    <h3 class="title animated wow zoomIn" data-wow-delay=".5s">Sobre nós</h3>
                    <p class="est animated wow zoomIn" data-wow-delay=".5s">Um pouco de nossa história.</p>

                    <div class="grid_3 grid_5 animated wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
                        <br>
                        <h3> Indústria Química Pulse</h3>
                        <br>

                        <div class="banner-bottom-grid-left animated wow slideInLeft" data-wow-delay=".10s" Style="margin-right: 10px">
                            <div class="grid">
                                <figure class="effect-julia">
                                    <img src="images/12.jpg" alt=" " class="img-responsive" />
                                </figure>
                            </div>
                        </div>

                        <div class="banner-bottom-grid animated wow slideInRight" data-wow-delay=".5s">
                            <div class="banner-bottom-grid-left-grid1">
                                <p> Fundada em 1977, na cidade de Santos, estado de São Paulo.
                                    É uma indústria de fabricação de suplementos alimentares e remédios para emagrecimento.
                                    Fornece produtos para grandes farmácias e lojas do mercado brasileiro, onde efetua suas vendas por
                                    esses meios como também pelo seu site (E-commerce).</p>
                                <p>Destaca-se por ser uma empresa que garante a qualidade de seus produtos e a satisfação dos seus clientes.</p>
                                <p>Sempre atualizada com as novidades do mercado, a Pulse também conta com tecnologia de ponta, 
                                    buscando otimização em seus processos de negócios, contanto com o gerenciamento completo da empresa, 
                                    dos seus funcionários e produtos, como também de seus clientes.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(__DIR__ . '/View/footer.php'); ?>
    </body>
</html>