<?php

class Grupos {

    protected $id;
    protected $descricao;

    function getId() {
        return $this->id;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function __construct($id, $descricao) {
        $this->id = $id;
        $this->descricao = $descricao;
    }

}
