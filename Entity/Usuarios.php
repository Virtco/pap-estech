<?php

class Usuarios {

    protected $id;
    protected $nome;
    protected $sobre_nome;
    protected $usuario;
    protected $senha;
    protected $id_cargos;

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getSobre_nome() {
        return $this->sobre_nome;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getSenha() {
        return $this->senha;
    }

    function getId_cargos() {
        return $this->id_cargos;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setSobre_nome($sobre_nome) {
        $this->sobre_nome = $sobre_nome;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setId_cargos($id_cargos) {
        $this->id_cargos = $id_cargos;
    }

    function __construct($id, $usuario, $senha, $id_cargos) {
        $this->id = $id;
        $this->usuario = $usuario;
        $this->senha = $senha;
        $this->id_cargos = $id_cargos;
    }

}
