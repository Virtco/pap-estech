<?php

class Pedidos {

    protected $id;
    protected $produtos_id;
    protected $clientes_id;

    function getId() {
        return $this->id;
    }

    function getProdutos_id() {
        return $this->produtos_id;
    }

    function getClientes_id() {
        return $this->clientes_id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setProdutos_id($produtos_id) {
        $this->produtos_id = $produtos_id;
    }

    function setClientes_id($clientes_id) {
        $this->clientes_id = $clientes_id;
    }

    function __construct($id, $produtos_id, $clientes_id) {
        $this->id = $id;
        $this->produtos_id = $produtos_id;
        $this->clientes_id = $clientes_id;
    }

}
