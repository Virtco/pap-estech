<?php include __DIR__ . '/Controller/Query.php'; ?>
<?php
$select = new Query();
$resultado = $select->executeQuery();
?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/View/head.php'; ?>
    <body>
        <?php include __DIR__ . '/View/header.php'; ?>
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Início</a></li>
                    <li class="active">Produtos</li>
                </ol>
            </div>
        </div>
        <div class="products">
            <div class="container">
                <div class="new-collections-grids">
                    <div class="products-right-grids-bottom">
                        <?php foreach ($resultado as $key => $value): ?>
                            <div class="col-md-4 products-right-grids-bottom-grid" style="margin-bottom:15px;">
                                <div class="new-collections-grid1 products-right-grid1 animated wow slideInUp" data-wow-delay=".5s">
                                    <div class="new-collections-grid1-image">
                                        <a href="single.php?id=<?= $value['id']; ?>">
                                            <img style="max-height: 150px;min-height: 150px;" 
                                                 src="https://tpws.com.br/imagens/admin/logo/produtos/<?= $value['imagem']; ?>" alt=" " class="img-responsive">
                                        </a>
                                        <div class="new-collections-grid1-image-pos products-center-grids-pos" style="margin-left:25px;">
                                            <a href="single.php?id=<?= $value['id']; ?>">Veja Mais</a>
                                        </div>
                                    </div>
                                    <h4><a><?= $value['nome']; ?></a></h4>
                                    <p><?= $value['descricao']; ?></p>
                                    <div class="simpleCart_shelfItem products-right-grid1-add-cart">
                                        <span class="item_image" style="display:none">https://tpws.com.br/imagens/admin/logo/produtos/<?= $value['imagem']; ?></span>
                                        <span class="item_name" style="display:none"><?= $value['nome']; ?></span>
                                        <p><span class="item_price">R$<?= number_format(floatval($value['preco']), 2); ?></span><a class="item_add" onclick="addProdutoNaSessao('<?= $value['id']; ?>');" href="#">
                                                Carrinho!</a></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <?php include(__DIR__ . '/View/footer.php'); ?>
    </body>
</html>