<?php
include __DIR__ . '/Mapper/Produtos.php';
include __DIR__ . '/Controller/Session.php';
?>
<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/View/head.php'; ?>
    <body>
        <?php include __DIR__ . '/View/header.php'; ?>        
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Início</a></li>
                    <li class="active">Carrinho</li>
                </ol>
            </div>
        </div>
        <div class="checkout">
            <div class="container">
                <h3 class="animated wow slideInLeft" data-wow-delay=".5s">Seu carrinho contém:  <span id="simpleCart_quantity" class="simpleCart_quantity"></span><span> Produtos</span></h3>
                <div class="checkout-right animated wow slideInUp" data-wow-delay=".5s">
                        <div class="simpleCart_items table" > </div>
                </div>
                <div class="checkout-left">	
                    <div class="checkout-left-basket animated wow slideInLeft" data-wow-delay=".5s">
                        <h4>Totais</h4>
                        <ul>
                            <li>Total <i>:</i> <span id="simpleCart_total" class="simpleCart_total"></span></li>
                            <li>Frete <i>:</i> <span id="simpleCart_shipping" class="simpleCart_shipping"></span></li>
                            <li>Taxação <i>:</i> <span id="simpleCart_tax" class="simpleCart_tax"></span></li>
                            <li>Quantidade <i>:</i> <span id="simpleCart_quantity" class="simpleCart_quantity"></span></li>
                            <li>Total Completo<i>:</i> <span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span></li>
                        </ul>
                    </div>
                    <div class="checkout-right-basket animated wow slideInRight" data-wow-delay=".5s">
                        <a href="products.php"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Continuar a compra!</a><br><br>
                        <a href="javascript:;" class="simpleCart_checkout"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Finalizar compra!</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
       <?php include(__DIR__ . '/View/footer.php'); ?>
       <script>
           $(document).ready(function () {
                $(".").validate({
                    
                });
            });

       </script>
    </body>
</html>