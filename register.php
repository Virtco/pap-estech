<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/View/head.php'; ?>
    <script type="text/javascript" src="js/register.js"></script>
    <body>
        <?php include __DIR__ . '/View/header.php'; ?>
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Início</a></li>
                    <li class="active">Pagina de Registro</li>
                </ol>
            </div>
        </div>
        <div class="register">
            <div class="container">
                <h3 class="animated wow zoomIn" data-wow-delay=".5s">Cadastre-se!</h3>
                <p class="est animated wow zoomIn" data-wow-delay=".5s">Cadastre-se agora em nosso site para adquirir nossos produtos e ser um membro Pulse!</p>
                <div id="mensagemCadastro" align="center" style="display:none" data-wow-delay=".5s">
                    <p id="resposta"></p>
                    <div class="register-home" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
                        <a href="login.php">Login</a>
                    </div>
                </div>
                <div id="divCadastro" class="login-form-grids">
                    <h5 class="animated wow slideInUp" data-wow-delay=".5s">Informações de cadastro</h5>
                    <form id="formularioCadastro" action="javascript:;" class="animated wow slideInUp" data-wow-delay=".5s">
                        <input type="text" name="primeiroNome" id="primeiroNome" placeholder="Primeiro nome..." required=" " >
                        <input type="text" name="ultimoNome" id="ultimoNome" placeholder="Ultimo nome..." required=" " >
                        <input type="text" name="cpf" id="cpf" maxlength="14" placeholder="CPF..." required=" " >
                        <input type="text" name="cep" id="cep" placeholder="CEP..." required=" " >
                        <input type="text" name="endereco" id="endereco" placeholder="Endereço..." required=" " >
                        <input type="text" name="numero" id="numero" placeholder="Número..." required=" " >
                        <input type="text" name="bairro" id="bairro" placeholder="Bairro..." required=" " >
                        <input type="text" name="cidade" id="cidade" placeholder="Cidade..." required=" " >
                        <input type="text" name="estado" id="estado" placeholder="Estado..." required=" " >
                        <input type="email" name="email" id="email" placeholder="Endereço de e-mail..." required=" " >
                        <input type="text" name="telefone" id="telefone" maxlength="11" placeholder="Telefone..." required=" " >
                        <input type="text" name="login" id="login" placeholder="Login..." required=" " >
                        <input type="password" name="senha" id="senha" placeholder="Senha..." required=" " >
                        <input type="submit" value="Registrar!">
                    </form>
                    <div class="register-home animated wow slideInUp" data-wow-delay=".5s">
                        <a href="index.php">Início</a>
                    </div>
                </div>
            </div>
        </div>
        <?php include(__DIR__ . '/View/footer.php'); ?>
    </body>
</html>