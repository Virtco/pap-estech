<!DOCTYPE html>
<html>
    <?php include __DIR__ . '/View/head.php'; ?>
    <script type="text/javascript" src="js/mail.js" ></script>
    <body>
      <?php include __DIR__ . '/View/header.php'; ?>
        <div class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Início</a></li>
                    <li class="active">Contato!</li>
                </ol>
            </div>
        </div>
        <div class="mail animated wow zoomIn" data-wow-delay=".5s">
            <div class="container">
                <h3>Contato</h3>
                <p class="est">Mande já suas dúvidas e opiniões e nosso time entratá em contato o mais rápido possivel!</p>
                <div class="mail-grids">
                    <div class="col-md-8 mail-grid-left animated wow slideInLeft" data-wow-delay=".5s">
                        <form id="formularioContato" action="javascript:;" class="animated wow slideInUp" data-wow-delay=".5s">
                            <input type="email" name="email" id="email" placeholder="E-mail" required="">
                            <input type="text" name="subject" id="subject" placeholder="Assunto" required="">
                            <textarea type="text" style="resize:none" id="text" name="message" placeholder="Mensagem..."/></textarea>
                            <br>
                            <input type="submit" value="Envie já!" >
                        </form>
                        <div id="mensagemContato" style="display:none" class="col-md-8" data-wow-delay=".5s">
                            <h1><p id="resposta"></p></h1><br>
                            <div class="register-home" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
                                <a href="index.php">Início</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mail-grid-right animated wow slideInRight" data-wow-delay=".5s">
                        <div class="mail-grid-right1">
                            <img src="images/3.png" alt=" " class="img-responsive" />
                            <h4>Camila Freitas <span>Gerente de Negócios</span></h4>
                            <ul class="phone-mail">
                                <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Telefone 11 4352 3245</li>
                                <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>E-mail: <a href="mailto:info@example.com">camila@iqpulse.com.br</a></li>
                            </ul>
                            <ul class="social-icons">
                                <li><a href="#" class="facebook"></a></li>
                                <li><a href="#" class="twitter"></a></li>
                                <li><a href="#" class="g"></a></li>
                                <li><a href="#" class="instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <iframe class="animated wow slideInLeft" data-wow-delay=".5s" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3654.637503394796!2d-46.57788918502051!3d-23.65314958463739!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce4347bbd6075b%3A0x9ed2265c58402851!2sR.+Alfeu+Tav%C3%A1res%2C+149+-+Vila+America%2C+S%C3%A3o+Bernardo+do+Campo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1510105286378" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="footer-grids">
                    <div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".5s">
                        <h3>Sobre nós</h3>
                        <p>Focada em inovações, a Pulse se dedica em fabricar suplementos alimentares e remédios para controle de peso de crianças e adultos de altíssima qualidade, destacando-se por oferecer seus produtos online, fornecimento de produtos para lojas e farmácias, e pelo gerenciamento de clientes, garantindo a satisfação e fidelização destes. </span></p>
                    </div>
                    <div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".6s">
                        <h3>Contato</h3>
                        <ul>
                            <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Avenida Senquior Helios, 324 <span>Cidade da Luz.</span></li>
                            <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:vendas@iqpulse.com.br">vendas@iqpulse.com.br</a></li>
                            <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+11 4356 5059</li>
                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".7s">
                        <h3>Instagram</h3>
                        <div class="footer-grid-left">
                            <img src="images/13.jpg" alt=" " class="img-responsive" /></a>
                        </div>
                        <div class="footer-grid-left">
                            <img src="images/14.jpg" alt=" " class="img-responsive" /></a>
                        </div>
                        <div class="footer-grid-left">
                            <img src="images/15.jpg" alt=" " class="img-responsive" /></a>
                        </div>
                        <div class="footer-grid-left">
                            <img src="images/1.jpg" alt=" " class="img-responsive" /></a>
                        </div>
                        <div class="footer-grid-left">
                            <img src="images/13.jpg" alt=" " class="img-responsive" /></a>
                        </div>
                        <div class="footer-grid-left">
                            <img src="images/14.jpg" alt=" " class="img-responsive" /></a>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".8s">
                        <h3>Postagens</h3>
                        <div class="footer-grid-sub-grids">
                            <div class="footer-grid-sub-grid-left">
                                <a href="single.php?id=6"><img src="images/produtos/cafeina.jpg" alt=" " class="img-responsive" /></a>
                            </div>
                            <div class="footer-grid-sub-grid-right">
                                <h4><a href="single.php?id=6">Base cafeinada!</a></h4>
                                <p>Post de 26/7/2017</p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="footer-grid-sub-grids">
                            <div class="footer-grid-sub-grid-left">
                                <a href="single.php?id=4"><img src="images/produtos/WHEY.jpg" alt=" " class="img-responsive" /></a>
                            </div>
                            <div class="footer-grid-sub-grid-right">
                                <h4><a href="single.php?id=4">WHEY Pulse vol3!</a></h4>
                                <p>Post de 25/3/2016</p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="footer-logo animated wow" data-wow-delay=".5s">
                    <h2><a href="index.php"> Seja Pulse <span>Suplementos de Qualidade</span></a></h2>
                </div>
                <div class="copy-right animated wow" data-wow-delay=".5s">
                    <p>&copy 2017 Indústria Química Pulse. Direitos reservados | Feito por <a href="http://google.com/">ESTechnology</a></p>
                </div>
            </div>
        </div>
    </body>
</html>