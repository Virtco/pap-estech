<!-- footer -->
<div class="footer">
    <div class="container">
        <div class="footer-grids">
            <div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".5s">
                <h3>Sobre nós</h3>
                <p>Focada em inovações, a Pulse se dedica em fabricar suplementos alimentares e remédios para controle de peso de crianças e adultos de altíssima qualidade, destacando-se por oferecer seus produtos online, fornecimento de produtos para lojas e farmácias, e pelo gerenciamento de clientes, garantindo a satisfação e fidelização destes. </span></p>
            </div>
            <div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".6s">
                <h3>Contato</h3>
                <ul>
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Avenida Senquior Helios, 324 <span>Cidade da Luz.</span></li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:vendas@iqpulse.com.br">vendas@iqpulse.com.br</a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+11 4356 5059</li>
                </ul>
            </div>
            <div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".7s">
                <h3>Instagram</h3>
                <div class="footer-grid-left">
                    <img src="images/13.jpg" alt=" " class="img-responsive" /></a>
                </div>
                <div class="footer-grid-left">
                    <img src="images/14.jpg" alt=" " class="img-responsive" /></a>
                </div>
                <div class="footer-grid-left">
                    <img src="images/15.jpg" alt=" " class="img-responsive" /></a>
                </div>
                <div class="footer-grid-left">
                    <img src="images/1.jpg" alt=" " class="img-responsive" /></a>
                </div>
                <div class="footer-grid-left">
                    <img src="images/13.jpg" alt=" " class="img-responsive" /></a>
                </div>
                <div class="footer-grid-left">
                    <img src="images/14.jpg" alt=" " class="img-responsive" /></a>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-3 footer-grid animated wow slideInLeft" data-wow-delay=".8s">
                <h3>Postagens</h3>
                <div class="footer-grid-sub-grids">
                    <div class="footer-grid-sub-grid-left">
                        <a href="single.php?id=6"><img src="images/produtos/cafeina.jpg" alt=" " class="img-responsive" /></a>
                    </div>
                    <div class="footer-grid-sub-grid-right">
                        <h4><a href="single.php?id=6">Base cafeinada!</a></h4>
                        <p>Post de 26/7/2017</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="footer-grid-sub-grids">
                    <div class="footer-grid-sub-grid-left">
                        <a href="single.php?id=4"><img src="images/produtos/WHEY.jpg" alt=" " class="img-responsive" /></a>
                    </div>
                    <div class="footer-grid-sub-grid-right">
                        <h4><a href="single.php?id=4">WHEY Pulse vol3!</a></h4>
                        <p>Post de 25/3/2016</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="footer-logo animated wow " data-wow-delay=".5s">
            <h2><a href="index.php"> Seja Pulse <span>Suplementos de Qualidade</span></a></h2>
        </div>
        <div class="copy-right animated wow" data-wow-delay=".5s">
            <p>&copy 2017 Indústria Química Pulse. Direitos reservados | Feito por <a href="http://google.com/">ESTechnology</a></p>
        </div>
    </div>
</div>
<!-- //footer -->
